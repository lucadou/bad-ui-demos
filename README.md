# Bad UI Demos

Occasionally I think up some absolutely terrible designs. This repo is to store them. Video demonstrations will be in the [demos folder](demos).

Please do not implement these in your applications, but if you do for some reason, do not give me credit. (I chose Unlicense specifically for the lack of attribution requirements.) This code is not intended to go into production, so don't expect it to be super high quality.

Current list of mistakes:

* Password Typeahead - it autocompletes your passwords. And everyone else's. Works best in Chrome (the password isn't censored properly in Firefox or Edge). [Demo](demos/password-typeahead.webm)
* Pourendous 2FA - 2FA code input...through a measuring cup puzzle! Pour one out to prove your identity and show you're a human. Like all great captchas, only ~~robots~~ the most quick-witted can solve it. [Demo](demos/pourendous-2fa.webm)

## Getting Started

### Prerequisites

* Any modern browser
* An HTTP server such as Nginx or just `python3 -m http.server`

### Installation

* Copy the files to your webserver
* Start your webserver, if needed
* Go to it in your web browser
* Revel in the awfulness

## FAQ

> Why?

There's nothing better than a joke gone too far.

> Demo recording process?

1. Record in OBS Studio (produces an MKV)
2. `ffmpeg -i recording.mkv -c:v libvpx -qmin 0 -crf 5 -b:v 1M -an recording.webm`  
   Notes:
   * `-c:v libvpx` - encode video stream with libvpx
   * `-qmin 0` - VP8 encoder quality (0-63, lower = better quality)
   * `-crf 5` - VP8 encoder bitrate (4-63, lower = better quality)
   * `-b:v 1M` - target bitrate (1 Mb/s)
   * `-an` - no audio track (if you need audio, use `-acodec libvorbis` instead)  
   [FFmpeg main documentation](https://ffmpeg.org/ffmpeg.html) | [VP8 documentation](https://trac.ffmpeg.org/wiki/Encode/VP8)
3. Copy to demos folder

> You didn't include a text message generator for Pourendous 2FA, what gives?

I texted my Google Voice number, there is no server-side component to that demo. Fun fact: Google Voice will stop delivering the same message after about 7 times in the span of a few minutes, but if you just change it slightly (e.g. adding a "." onto the end or adding a "Hello!" at the front), you can spam away some more until you have to change it again. You might have to send something else (like a random emoji) in between messages to ensure it is delievered.

> How did you make the bars in the Pourendous 2FA demo video?

I added this into my ffmpeg command, after `-an`:
```
-vf "drawbox=x=975:y=625:w=305:h=22:color=black:t=fill:enable='between(t,6.750,31.950)', drawbox=x=975:y=670:w=305:h=22:color=black:t=fill:enable='between(t,6.750,31.950)'"
```

[Drawbox documentation](https://ffmpeg.org/ffmpeg-filters.html#drawbox)

## License

This project is licensed under the Unlicense - see the [LICENSE.md](LICENSE.md) file for details.
