'use strict';

function preload_image(image_url) {
  let img = new Image();
  img.src = image_url;
};

window.onload = function(e) {
  // Preload dollar bill image
  // Image source: https://commons.wikimedia.org/wiki/File:US_one_dollar_bill,_obverse,_series_2009.jpg
  let dollar_img_url = 'dollarbill.jpg';
  preload_image(dollar_img_url);

  let money = Number.parseInt(document.getElementById('account-balance').innerHTML);
  let dollar_template = '<img class="img-fluid w-25" src="dollarbill.jpg" alt="Image of a United States one (1) dollar bill">';
  let loading_template = '<div class="spinner-border spinner-border-sm" role="status"><span class="sr-only">Loading...</span></div>';
  let sender_dollars = document.getElementById('sender-dollars-card');
  let receiver_dollars = document.getElementById('receiver-dollars-card');

  // On the click of any item in the list group of contacts, show the loading spinner for that element, then after a couple seconds, hide that div and un-hide the money drag and drop div

  // Populate dollars in transfer box
  // insert one at a time AFTER showing the card
  // and add a random delay between each, like it's verifying that you have another dollar in the account or something
  // display a loading spinner at each one

  // Drag and drop: fairly easy, actually
  // https://www.w3schools.com/html/html5_draganddrop.asp
};
