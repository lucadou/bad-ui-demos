'use strict';

function getPasswordSuggestions(current_entry, case_sensitive, password_list) {
  let empty_list = '<a href="#" class="list-group-item list-group-item-action disabled text-body font-weight-bold p-2" data-from="{1}">Unable to load password suggestions :(</a>';
  let suggestion_dym = '<a href="#" class="list-group-item list-group-item-action disabled text-body font-weight-bold p-2" data-from="{1}">Did you mean...</a>';
  let suggestion_template = '<a href="#" class="list-group-item list-group-item-action password-suggestion" data-from="{1}">{}</a>';
  let not_found = '<a href="#" class="list-group-item list-group-item-action disabled text-body font-italic p-2" data-from="{1}">Password not found</a>';

  if (password_list.length === 0) {
    return [empty_list.split('{1}').join(current_entry)];
  }

  let matching_list = password_list.filter(entry => entry.toLowerCase().indexOf(current_entry.toLowerCase()) >= 0);
  if (matching_list.length > 0) {
    return Array.prototype.concat(
      suggestion_dym.split('{1}').join(current_entry),
      Array.from(
        matching_list,
        function(entry) {
          let start_index = entry.toLowerCase().indexOf(current_entry.toLowerCase());
          let end_index = start_index + current_entry.length;
          entry = [entry.substring(0, start_index),
            '<span class="font-weight-bold">',
            entry.substring(start_index, end_index),
            '</span>',
            entry.substring(end_index)
          ].join('');
          return suggestion_template.split('{}').join(entry).split('{1}').join(current_entry)
        }
      )
    );
  } else {
    return [not_found.split('{1}').join(current_entry)];
  }
}

window.onload = function(e) {
  let username_field = document.getElementById('username');
  let password_field = document.getElementById('password');
  let password_suggestions = document.getElementById('password-typeahead');
  let account_list = {};
  let password_list = [];
  let max_suggestions = 5;
  let login_form = document.getElementById('login-form');
  let login_form_card = document.getElementById('login-form-card');
  let login_success_card = document.getElementById('login-success-card');
  let username_incorrect_message = document.getElementById('username-incorrect');
  let password_incorrect_message = document.getElementById('password-incorrect');
  let navbar = document.getElementById('navbar');

  // Load values
  let db_path = "database.json";
  let db_xhr = new XMLHttpRequest();
  db_xhr.addEventListener('load', function() {
    if (db_xhr.status === 200) {
      account_list = JSON.parse(db_xhr.responseText).accounts;
      password_list = Object.values(account_list);
      console.log('Loaded list');
    } else {
      console.log('Error :(');
    }
  });
  db_xhr.open("GET", db_path);
  db_xhr.send();

  password_suggestions.addEventListener('click', function(e) {
    /* I cannot specify an event listener for elements that have not yet been
     * added to the DOM, so I have to monitor events to the parent container.
     * https://dev.to/akhil_001/adding-event-listeners-to-the-future-dom-elements-using-event-bubbling-3cp1
     */
    if (e.target.matches('.password-suggestion')) {
      password_field.value = e.target.text;
      password_suggestions.classList.add('d-none');
    }
    e.stopPropagation();
  }, true);

  let password_field_suggestor = function(e) {
    /* Make sure the list has not already been populated
     * e.g. keyup -> clicking on suggestion would trigger change event listener
     */
    if (password_suggestions.children[0].getAttribute('data-from') === password_field.value) {
      // The list has already been populated, no need to run a second time
      return;
    }

    // Construct suggestion list
    let overflow_template = '<a href="#" class="list-group-item list-group-item-action disabled font-italic p-2">{} result(s) not shown</a>';
    let search_case_sensitive = false;
    let list = getPasswordSuggestions(password_field.value, search_case_sensitive, password_list);
    let slice_count = list.length - max_suggestions - 1; // -1 to account for the first element

    if (slice_count > 0) {
      password_suggestions.innerHTML = list.slice(0, max_suggestions + 1).join('') + overflow_template.split('{}').join(slice_count);
    } else {
      password_suggestions.innerHTML = list.slice(0, max_suggestions + 1).join('');
    }

    let delay = Math.floor(Math.random() * 500 + 500); // 0.5 to 1 second delay
    setTimeout(function(){
      password_suggestions.classList.remove('d-none'); // Unhide the list
    }, 1000); // Small delay
  }
  password_field.addEventListener('change', password_field_suggestor, false);
  password_field.addEventListener('keyup', password_field_suggestor, false);

  // The pièce de résistance
  login_form.addEventListener('submit', function(e) {
    e.preventDefault();
    e.target.lastElementChild.children[0].classList.add('disabled');
    username_incorrect_message.classList.add('d-none');
    password_incorrect_message.classList.add('d-none');
    if (account_list[username_field.value] === password_field.value) {
      setTimeout(function(){
        navbar.classList.add('d-none');
        login_form_card.classList.add('d-none');
      }, 1000); // Small delay
      setTimeout(function(){
        login_success_card.classList.remove('d-none');
      }, 2300); // Small delay
    } else if (account_list[username_field.value] === undefined) {
      setTimeout(function(){
        username_incorrect_message.classList.remove('d-none');
        e.target.lastElementChild.children[0].classList.remove('disabled');
      }, 1000); // Small delay
    } else {
      setTimeout(function(){
        password_incorrect_message.classList.remove('d-none');
        e.target.lastElementChild.children[0].classList.remove('disabled');
      }, 1000); // Small delay
    }
  });
}
