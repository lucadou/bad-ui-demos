'use strict';

function decrement_counter(element) {
  let current = Number.parseInt(element.innerHTML);
  if (current > 0) {
    element.innerHTML = current - 1;
  }
};

function prepare_c1() {
  let c1 = {};
  c1.min = 0;
  c1.max = Number.parseInt(document.getElementById('c1-capacity').innerHTML);
  c1.name = "Container 1";
  c1.bar = document.getElementById('c1-progress-bar');
  c1.capacity = document.getElementById('c1-capacity');
  c1.current_level = document.getElementById('c1-level');
  c1.empty_btn = document.getElementById('c1-dump');
  c1.fillA_btn = document.getElementById('c1-fill-c2');
  c1.fillB_btn = document.getElementById('c1-fill-c3');
  c1.fillC_btn = document.getElementById('c1-fill-c4');
  c1.pourA_btn = document.getElementById('c1-pour-c2');
  c1.pourB_btn = document.getElementById('c1-pour-c3');
  c1.pourC_btn = document.getElementById('c1-pour-c4');
  return c1;
};

function prepare_c2() {
  let c2 = {};
  c2.min = 0;
  c2.max = Number.parseInt(document.getElementById('c2-capacity').innerHTML);
  c2.name = "Container 2";
  c2.bar = document.getElementById('c2-progress-bar');
  c2.capacity = document.getElementById('c2-capacity');
  c2.current_level = document.getElementById('c2-level');
  c2.empty_btn = document.getElementById('c2-dump');
  c2.fillA_btn = document.getElementById('c2-fill-c1');
  c2.fillB_btn = document.getElementById('c2-fill-c3');
  c2.fillC_btn = document.getElementById('c2-fill-c4');
  c2.pourA_btn = document.getElementById('c2-pour-c1');
  c2.pourB_btn = document.getElementById('c2-pour-c3');
  c2.pourC_btn = document.getElementById('c2-pour-c4');
  return c2;
};
function prepare_c3() {
  let c3 = {};
  c3.min = 0;
  c3.max = Number.parseInt(document.getElementById('c3-capacity').innerHTML);
  c3.name = "Container 3";
  c3.bar = document.getElementById('c3-progress-bar');
  c3.capacity = document.getElementById('c3-capacity');
  c3.current_level = document.getElementById('c3-level');
  c3.empty_btn = document.getElementById('c3-dump');
  c3.fillA_btn = document.getElementById('c3-fill-c1');
  c3.fillB_btn = document.getElementById('c3-fill-c2');
  c3.fillC_btn = document.getElementById('c3-fill-c4');
  c3.pourA_btn = document.getElementById('c3-pour-c1');
  c3.pourB_btn = document.getElementById('c3-pour-c2');
  c3.pourC_btn = document.getElementById('c3-pour-c4');
  return c3;
};
function prepare_c4() {
  let c4 = {};
  c4.min = 0;
  c4.max = Number.parseInt(document.getElementById('c4-capacity').innerHTML);
  c4.name = "Container 4";
  c4.bar = document.getElementById('c4-progress-bar');
  c4.capacity = document.getElementById('c4-capacity');
  c4.current_level = document.getElementById('c4-level');
  c4.empty_btn = document.getElementById('c4-dump');
  c4.code_field = document.getElementById('2facode');
  return c4;
};

function empty(c_to_empty, status_message, containers=null) {
  // Check container name
  if (c_to_empty.name === "Container 4") {
    // Append the value to Code box
    c_to_empty.code_field.value += c_to_empty.current_level.innerHTML;
    if (document.getElementById('c4-progress-cell-reg').classList.contains('d-none')) {
      // Show regular progress bar's table cell to reset the container
      document.getElementById('c4-progress-cell-reg').classList.remove('d-none');
      // Hide spilled progress bar's table cell
      document.getElementById('c4-progress-cell-spill').classList.add('d-none');
    }
    if (containers !== null) {
      // Update all containers
      for (let i = 0; i < containers.length; i++) {
        let cur_container = containers[i];
        if (cur_container.name !== "Container 4") {
          // Update progress bars and capacity of to_empty
          cur_container.bar.setAttribute('aria-valuenow', cur_container.max);
          cur_container.bar.style.height = '100%';
          cur_container.current_level.innerHTML = cur_container.max;
        }
      }
      // Update status message
      status_message.innerHTML = `Appended ${c_to_empty.current_level.innerHTML} to the Code and reset the containers.`;
    } else {
      // This method should have been called with containers provided
      // Update status message
      status_message.innerHTML = `Appended ${c_to_empty.current_level.innerHTML} to the Code, but could not reset the containers.`;
    }
  } else {
    // Make sure to_empty has something to dump out
    // Update the status message
    if (c_to_empty.current_level.innerHTML === "0") {
      status_message.innerHTML = 'Error: Cannot empty an empty container.';
    } else {
      status_message.innerHTML = `Emptied ${c_to_empty.name}.`;
    }
  }

  // Update progress bars and capacity of to_empty
  c_to_empty.bar.setAttribute('aria-valuenow', 0);
  c_to_empty.bar.style.height = '0%';
  c_to_empty.current_level.innerHTML = '0';
}

function fill(c_to_fill, c_to_drain, status_message, filling=true) {
  // Make sure to_drain has something in it
  if (Number.parseInt(c_to_drain.current_level.innerHTML) === 0) {
    status_message.innerHTML = `Error: ${c_to_drain.name} is empty.`;
  } else if (c_to_fill.name === "Container 4" && document.getElementById('c4-progress-cell-reg').classList.contains('d-none')) {
    // C4 has been spilled
    // Update status message
    status_message.innerHTML = `Error: ${c_to_fill.name} has been spilled.`;
  } else if (Number.parseInt(c_to_fill.current_level.innerHTML) < c_to_fill.max) { // Make sure to_fill has capacity
    // Dump from to_drain until to_fill is full or to_drain is empty
    let space_left = c_to_fill.max - Number.parseInt(c_to_fill.current_level.innerHTML);
    if (space_left >= Number.parseInt(c_to_drain.current_level.innerHTML)) {
      // Drain entire container
      let new_fill = Number.parseInt(c_to_fill.current_level.innerHTML) + Number.parseInt(c_to_drain.current_level.innerHTML);
      // Update progress bars
      c_to_fill.bar.setAttribute('aria-valuenow', new_fill);
      c_to_drain.bar.setAttribute('aria-valuenow', 0);
      c_to_fill.bar.style.height = `${(new_fill / c_to_fill.max) * 100}%`;
      c_to_drain.bar.style.height = '0%';
      // Update capacity
      c_to_fill.current_level.innerHTML = new_fill;
      c_to_drain.current_level.innerHTML = '0';
      // Update status message
      if (filling) {
        status_message.innerHTML = `Filled ${c_to_fill.name} from ${c_to_drain.name}.`;
      } else {
        status_message.innerHTML = `Poured ${c_to_drain.name} into ${c_to_fill.name}.`;
      }
    } else {
      // Only drain until full
      let new_ctf_level = Number.parseInt(c_to_fill.current_level.innerHTML) + space_left;
      let new_ctd_level = Number.parseInt(c_to_drain.current_level.innerHTML) - space_left;
      // Update progress bars
      c_to_fill.bar.setAttribute('aria-valuenow', new_ctf_level);
      c_to_drain.bar.setAttribute('aria-valuenow', new_ctd_level);
      c_to_fill.bar.style.height = `${(new_ctf_level / c_to_fill.max) * 100}%`;
      c_to_drain.bar.style.height = `${(new_ctd_level / c_to_drain.max) * 100}%`;
      // Update capacity
      c_to_fill.current_level.innerHTML = new_ctf_level;
      c_to_drain.current_level.innerHTML = new_ctd_level;
      // Update status message
      if (filling) {
        status_message.innerHTML = `Filled ${space_left} in ${c_to_fill.name} from ${c_to_drain.name}.`;
      } else {
        status_message.innerHTML = `Poured ${space_left} from ${c_to_drain.name} to ${c_to_fill.name}.`;
      }
    }
  } else {
    // Full - cannot pour anything
    status_message.innerHTML = `Error: ${c_to_fill.name} is full.`;
  }
};

function pour(c_to_drain, c_to_fill, status_message) {
  fill(c_to_fill, c_to_drain, status_message, false);
  // Make sure to_fill has capacity
  // Dump from to_drain until to_fill is full or to_drain is empty
  // Update progress bars and capacity of to_drain
  // Update progress bars and capacity of to_fill
  // Update the status message
}

function connectButtons(c1, c2, c3, c4, containers, status_message) {
  // Empty buttons
  c1.empty_btn.addEventListener('click', function() {
    empty(c1, status_message);
  });
  c2.empty_btn.addEventListener('click', function() {
    empty(c2, status_message);
  });
  c3.empty_btn.addEventListener('click', function() {
    empty(c3, status_message);
  });
  c4.empty_btn.addEventListener('click', function() {
    empty(c4, status_message, containers);
  });
  // Fill buttons
  c1.fillA_btn.addEventListener('click', function() {
    fill(c1, c2, status_message);
  });
  c2.fillA_btn.addEventListener('click', function() {
    fill(c2, c1, status_message);
  });
  c3.fillA_btn.addEventListener('click', function() {
    fill(c3, c1, status_message);
  });
  c1.fillB_btn.addEventListener('click', function() {
    fill(c1, c3, status_message);
  });
  c2.fillB_btn.addEventListener('click', function() {
    fill(c2, c3, status_message);
  });
  c3.fillB_btn.addEventListener('click', function() {
    fill(c3, c2, status_message);
  });
  // Pour buttons
  c1.pourA_btn.addEventListener('click', function() {
    pour(c1, c2, status_message);
  });
  c2.pourA_btn.addEventListener('click', function() {
    pour(c2, c1, status_message);
  });
  c3.pourA_btn.addEventListener('click', function() {
    pour(c3, c1, status_message);
  });
  c1.pourB_btn.addEventListener('click', function() {
    pour(c1, c3, status_message);
  });
  c2.pourB_btn.addEventListener('click', function() {
    pour(c2, c3, status_message);
  });
  c3.pourB_btn.addEventListener('click', function() {
    pour(c3, c2, status_message);
  });
  c1.pourC_btn.addEventListener('click', function() {
    pour(c1, c4, status_message);
  });
  c2.pourC_btn.addEventListener('click', function() {
    pour(c2, c4, status_message);
  });
  c3.pourC_btn.addEventListener('click', function() {
    pour(c3, c4, status_message);
  });
};

window.onload = function(e) {
  let code_field = document.getElementById('2facode');
  let code_length = 6;
  let timer_span = document.getElementById('timer-2fa');
  let login_form = document.getElementById('login-form');
  let login_form_card = document.getElementById('login-form-card');
  let login_success_card = document.getElementById('login-success-card');
  let code_expired_message = document.getElementById('code-expired');
  let code_incorrect_message = document.getElementById('code-incorrect');
  let navbar = document.getElementById('navbar');
  let status_text = document.getElementById('status-message');

  // Setup measure controls
  let c1 = prepare_c1();
  let c2 = prepare_c2();
  let c3 = prepare_c3();
  let c4 = prepare_c4();
  let containers = [c1, c2, c3, c4];
  connectButtons(c1, c2, c3, c4, containers, status_text);

  // Easter egg
  document.getElementById('c4-progress-bar-container').addEventListener('mouseenter', function() {
    // Hide regular progress bar's table cell
    document.getElementById('c4-progress-cell-reg').classList.add('d-none');
    // Show spilled progress bar's table cell
    document.getElementById('c4-progress-cell-spill').classList.remove('d-none');
    // Set spilled progress bar to regular progress bar's value
    let c4_level = Number.parseInt(c4.current_level.innerHTML);
    let spilled_c4_bar = document.getElementById('c4-progress-spilled');
    // No need to set aria-valuenow, as the usable level is 0
    if (c4_level > 0) {
      spilled_c4_bar.style.width = '100%';
      // No need to fill the spilled bar if it was already empty
    }
    // Set capacity to 0
    c4.current_level.innerHTML = '0';
    // Update status message
    status_text.innerHTML = 'Uh-oh, you spilled Container 4! To get it upright again, press "Enter Number" or click <a href="/">here</a> for a new code.';
  });

  // Wait 1 seocnd, then start countdown timer
  window.setTimeout(function() {
    let timer = window.setInterval(function() {
      decrement_counter(timer_span);
    }, 1000); // Update every second
  }, 1000);

  // The pièce de résistance
  login_form.addEventListener('submit', function(e) {
    e.preventDefault();
    e.target.lastElementChild.children[0].classList.add('disabled');
    code_expired_message.classList.add('d-none');
    code_incorrect_message.classList.add('d-none');
    if (code_field.value.length === code_length && Number.parseInt(timer_span.innerHTML) > 0) {
      window.setTimeout(function(){
        navbar.classList.add('d-none');
        login_form_card.classList.add('d-none');
      }, 1000); // Small delay
      window.setTimeout(function(){
        login_success_card.classList.remove('d-none');
      }, 2300); // Small delay
    } else if (Number.parseInt(timer_span.innerHTML) == 0) {
      window.setTimeout(function(){
        code_expired_message.classList.remove('d-none');
        e.target.lastElementChild.children[0].classList.remove('disabled');
      }, 1000); // Small delay
    } else {
      window.setTimeout(function(){
        code_incorrect_message.classList.remove('d-none');
        e.target.lastElementChild.children[0].classList.remove('disabled');
      }, 1000); // Small delay
    }
  });
};
